import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PagarPage } from 'src/app/pages/contas/pagar/pagar.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { ReceberPage } from './receber/receber.page';
import { RelatorioPage } from './relatorio/relatorio.page';

const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: PagarPage},
      {path: 'receber', component: ReceberPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PagarPage,
    ReceberPage,
    CadastroPage,
    RelatorioPage
  ]
})
export class ContasRoutingModule { }
