// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAzYOEGyMnh3NjK_YlI-BiOfK52A9XbuxY',
    authDomain: 'controle-if-thiago.firebaseapp.com',
    databaseURL: 'https://controle-if-thiago.firebaseio.com',
    projectId: 'controle-if-thiago',
    storageBucket: 'controle-if-thiago.appspot.com',
    messagingSenderId: '155576459666',
    appId: '1:155576459666:web:91573733db5b627fe7e14b',
    measurementId: 'G-XFJBZE8JKP'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
